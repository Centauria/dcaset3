# -*- coding: utf-8 -*-
import os
import csv
import optuna
import argparse
import logging
import keras
import numpy as np
from functools import partial
from typing import Union
import models
import parameter
import cls_data_generator
import cls_feature_class
from metrics import evaluation_metrics, SELD_evaluation_metrics
from util import seld_metric

logging.basicConfig(level=logging.INFO)


def opt_dyn_thresh(model_or_result: Union[list, np.ndarray, keras.Model],
                   data_gen_test, params, feat_cls, search_range, step,
                   trial: optuna.trial.Trial):
    nb_classes = data_gen_test.get_nb_classes()
    if type(model_or_result) == keras.Model:
        pred_test = model_or_result.predict_generator(
            generator=data_gen_test.generate(),
            steps=2 if params['quick_test'] else data_gen_test.get_total_batches_in_data(),
            verbose=2
        )
    elif type(model_or_result) in (np.ndarray, list):
        pred_test = np.array(model_or_result)
        pred_test = (pred_test[:, :, :14], pred_test[:, :, 14:])
    else:
        raise TypeError
    dyn_thresh = [trial.suggest_float('th_{}'.format(i), th_min, th_max, step=step)
                  for i, (th_min, th_max) in enumerate(search_range)]
    test_sed_pred = evaluation_metrics.reshape_3Dto2D(pred_test[0]) > dyn_thresh
    test_doa_pred = evaluation_metrics.reshape_3Dto2D(
        pred_test[1] if (params['doa_objective'] is 'mse' or type(model_or_result) in (np.ndarray, list))
        else pred_test[1][:, :, nb_classes:]
    )

    test_new_metric, test_new_seld_metric, test_sed_loss, test_doa_loss = \
        seld_metric(data_gen_test, test_sed_pred, test_doa_pred, params, feat_cls)
    logging.info(
        'DOA: DOA Error: {:0.3f}, F-score: {:0.3f}; SED: Error rate: {:0.3f}, F-score: {:0.3f}; SELD (early): {:0.5f}'
            .format(
            test_new_metric[2],
            test_new_metric[3] * 100,
            test_new_metric[0],
            test_new_metric[1] * 100,
            test_new_seld_metric
        )
    )
    return test_new_seld_metric


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--task_id', default='default', help='User-defined parameter set from parameter.py')
    parser.add_argument('--job_id', default='1',
                        help='Unique identifier which is used for output filenames (models, training plots).'
                             + os.linesep +
                             'You can use any number or string for this.')
    parser.add_argument('--result_dir', help='Result directory' + os.linesep +
                                             'If specified, will not use model.')
    parser.add_argument('-t', '--trial', type=int, default=1000, help='Your trial chance.')
    parser.add_argument('-s', '--step', type=float, default=0.05, help='Threshold search step.')
    parser.add_argument('-f', '--file', help='A csv file that contains the threshold search range.')
    args = parser.parse_args()

    params = parameter.load_parameters(args.task_id)
    feat_cls = cls_feature_class.FeatureClass(params)
    unique_name = '{}_{}_{}_{}_split{}'.format(
        args.task_id, args.job_id, params['dataset'], params['mode'], 1
    )
    unique_name = os.path.join(params['model_dir'], unique_name)
    data_gen_test = cls_data_generator.DataGenerator(
        params=params, split=1, shuffle=False, per_file=params['dcase_output'],
        is_eval=True if params['mode'] is 'eval' else False
    )

    if args.result_dir is None:
        model_or_result = models.load_seld_model('{}_model.h5'.format(unique_name), params['doa_objective'])
    else:
        model_or_result = []
        for result_file in os.listdir(args.result_dir):
            model_or_result.append(np.load(os.path.sep.join((args.result_dir, result_file))))

    search_range = []
    with open(args.file, newline='') as csv_file:
        reader = csv.DictReader(csv_file)
        for row in reader:
            search_range.append(tuple(map(float, row.values())))
    study = optuna.create_study()
    study.optimize(partial(opt_dyn_thresh, model_or_result, data_gen_test, params, feat_cls, search_range, args.step),
                   n_trials=args.trial)
    print('Best result: {}'.format(study.best_value))
    print('Best params: {}'.format(study.best_params))
