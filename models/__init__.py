# -*- coding: utf-8 -*-
from keras.models import load_model

from .seld import get_model_seld
from .resnet_rnn import get_model_resnet
from .resnet_rnn_qwang import get_model_resnet_qwang
from .resnet_rnn_concat import get_model_resnet_concat
from .resnet_rnn_concat_conv import get_model_resnet_concat_conv
from .resnet_rnn_concat_convw import get_model_resnet_concat_convw
from .xception_rnn import get_model_xception_rnn
from .utility import masked_mse


def get_model(name, data_in, data_out, params):
    if name == 'seld':
        return get_model_seld(
            data_in, data_out,
            dropout_rate=params['dropout_rate'],
            num_filters=params['num_filters'],
            f_pool_size=params['f_pool_size'],
            t_pool_size=params['t_pool_size'],
            rnn_size=params['rnn_size'],
            fnn_size=params['fnn_size'],
            weights=params['loss_weights'],
            doa_objective=params['doa_objective']
        )
    elif name == 'resnet-rnn':
        return get_model_resnet(
            data_in, data_out,
            dropout_rate=params['dropout_rate'],
            rnn_size=params['rnn_size'],
            fnn_size=params['fnn_size'],
            weights=params['loss_weights'],
            doa_objective=params['doa_objective']
        )
    elif name == 'resnet-rnn-qwang':
        return get_model_resnet_qwang(
            data_in, data_out,
            dropout_rate=params['dropout_rate'],
            f_pool_size=params['f_pool_size'],
            t_pool_size=params['t_pool_size'],
            rnn_size=params['rnn_size'],
            wd=params['wd'],
            fnn_size=params['fnn_size'],
            num_filters=params['num_filters'],
            weights=params['loss_weights'],
            doa_objective=params['doa_objective']
        )
    elif name == 'resnet-rnn-concat':
        return get_model_resnet_concat(
            data_in, data_out,
            dropout_rate=params['dropout_rate'],
            f_pool_size=params['f_pool_size'],
            t_pool_size=params['t_pool_size'],
            rnn_size=params['rnn_size'],
            wd=params['wd'],
            fnn_size=params['fnn_size'],
            num_filters=params['num_filters'],
            weights=params['loss_weights'],
            doa_objective=params['doa_objective']
        )
    elif name == 'resnet-rnn-concat-conv':
        return get_model_resnet_concat_conv(
            data_in, data_out,
            dropout_rate=params['dropout_rate'],
            f_pool_size=params['f_pool_size'],
            t_pool_size=params['t_pool_size'],
            rnn_size=params['rnn_size'],
            wd=params['wd'],
            fnn_size=params['fnn_size'],
            num_filters=params['num_filters'],
            num_res_blocks=params['num_res_blocks'],
            weights=params['loss_weights'],
            doa_objective=params['doa_objective'],
            doa_tanh=params['doa_tanh']
        )
    elif name == 'resnet-rnn-concat-convw':
        return get_model_resnet_concat_convw(
            data_in, data_out,
            dropout_rate=params['dropout_rate'],
            f_pool_size=params['f_pool_size'],
            t_pool_size=params['t_pool_size'],
            rnn_size=params['rnn_size'],
            wd=params['wd'],
            fnn_size=params['fnn_size'],
            num_filters=params['num_filters'],
            weights=params['loss_weights'],
            doa_objective=params['doa_objective']
        )
    elif name == 'xception-rnn':
        return get_model_xception_rnn(
            data_in, data_out,
            dropout_rate=params['dropout_rate'],
            rnn_size=params['rnn_size'],
            fnn_size=params['fnn_size'],
            mid_block_num=params['mid_block_num'],
            block_13=params['block_13'],
            block_14=params['block_14'],
            mid_filter_num=params['mid_filter_num'],
            wd=params['wd'],
            weights=params['loss_weights'],
            doa_objective=params['doa_objective']
        )


def load_seld_model(model_file, doa_objective):
    if doa_objective == 'mse':
        return load_model(model_file)
    elif doa_objective == 'masked_mse':
        return load_model(model_file, custom_objects={'masked_mse': masked_mse})
    else:
        print('ERROR: Unknown doa objective: {}'.format(doa_objective))
        exit()
