# -*- coding: utf-8 -*-
from keras.layers import BatchNormalization, Activation, MaxPooling2D, AveragePooling2D, Conv2D, SeparableConv2D, Lambda
from keras.regularizers import l2
import keras
from typing import Optional

from .utility import pad_depth


def resnet_layer(
        inputs,
        num_filters=16,
        kernel_size=3,
        strides=1,
        learn_bn=True,
        wd=1e-4,
        use_relu=True,
        pool_size=None):
    x = inputs
    x = BatchNormalization(center=learn_bn, scale=learn_bn)(x)
    if use_relu:
        x = Activation('relu')(x)
    if pool_size is not None:
        x = MaxPooling2D(pool_size=pool_size, padding='same')(x)
    x = Conv2D(num_filters, kernel_size=kernel_size, strides=strides, padding='same', kernel_initializer='he_normal',
               kernel_regularizer=l2(wd), use_bias=False)(x)
    return x


def resnet_block(inputs, num_res_blocks, num_filters, pool_size=None, wd=1e-4):
    residual_path = inputs
    for res_block in range(num_res_blocks):
        if res_block == 0 and pool_size is not None:
            conv_path = resnet_layer(
                inputs=residual_path,
                num_filters=num_filters,
                learn_bn=False,
                wd=wd,
                use_relu=True,
                pool_size=pool_size
            )
        else:
            conv_path = resnet_layer(
                inputs=residual_path,
                num_filters=num_filters,
                learn_bn=False,
                wd=wd,
                use_relu=True
            )
        conv_path = resnet_layer(
            inputs=conv_path,
            num_filters=num_filters,
            learn_bn=False,
            wd=wd,
            use_relu=True
        )
        if res_block == 0 and pool_size is not None:
            residual_path = AveragePooling2D(pool_size=pool_size, padding='same')(residual_path)
            desired_channels = conv_path.shape.as_list()[-1]
            padding = Lambda(pad_depth, arguments={'desired_channels': desired_channels})(residual_path)
            residual_path = keras.layers.Concatenate(axis=-1)([residual_path, padding])
        residual_path = keras.layers.add([conv_path, residual_path])
    return residual_path


def universal_conv(inputs, num_filters, wd=1e-4):
    return Conv2D(num_filters, kernel_size=1, padding='same', kernel_initializer='he_normal',
                  kernel_regularizer=l2(wd), use_bias=False)(inputs)


def sep_conv(inputs, num_filters, activation: Optional[str] = 'relu', wd: Optional[float] = None):
    if wd is None:
        x = SeparableConv2D(num_filters, (3, 3), padding='same', use_bias=False)(inputs)
    else:
        x = SeparableConv2D(num_filters, (3, 3), padding='same', kernel_regularizer=l2(wd), use_bias=False)(inputs)
    x = BatchNormalization()(x)
    if activation is not None:
        x = Activation(activation)(x)
    return x
