# -*- coding: utf-8 -*-
from functools import partial
from keras.layers import Bidirectional, MaxPooling2D, AveragePooling2D, Input, Concatenate, Lambda
from keras.layers.core import Dense, Activation, Dropout, Reshape, Permute
from keras.layers.recurrent import GRU
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.layers.wrappers import TimeDistributed
from keras.optimizers import Adam
import keras

from .layers import resnet_layer
from .utility import masked_mse, pad_depth

keras.backend.set_image_data_format('channels_first')


def split_1(x):
    return x[:, :, :, :64]


def split_2(x):
    return x[:, :, :, 64:128]


def resnet_stack(inputs, num_filters, kernel_size=3, strides=1, learn_bn=True, wd=1e-4, use_relu=True):
    pass


def resnet(num_classes, input_shape=(128, None, 2), num_filters=24, wd=1e-3):
    my_wd = wd  # this is 5e-3 in matlab, so quite large
    num_res_blocks = 2

    inputs = Input(shape=input_shape)  # 用来实例化一个keras张量tensor，不包括batch size

    # split up frequency into two branches
    split1 = Lambda(split_1)(inputs)  # 用Lambda创建keras.layers中没有的层
    split2 = Lambda(split_2)(inputs)

    residual_path1 = resnet_layer(inputs=split1,
                                  num_filters=num_filters,
                                  learn_bn=True,
                                  wd=my_wd,
                                  use_relu=False)

    residual_path2 = resnet_layer(inputs=split2,
                                  num_filters=num_filters,
                                  learn_bn=True,
                                  wd=my_wd,
                                  use_relu=False)

    # Instantiate the stack of residual units
    for stack in range(4):
        resnet_layer_default = partial(resnet_layer,
                                       num_filters=num_filters,
                                       learn_bn=False,
                                       wd=my_wd,
                                       use_relu=True)
        for res_block in range(num_res_blocks):
            conv_path1 = resnet_layer_default(inputs=residual_path1)
            conv_path2 = resnet_layer_default(inputs=residual_path2)
            conv_path1 = resnet_layer_default(inputs=conv_path1)
            conv_path2 = resnet_layer_default(inputs=conv_path2)
            if stack > 0 and res_block == 0:
                # first layer but not first stack:
                # this is where we have gone up in channels and down in feature map size
                # so need to account for this in the residual path
                # average pool and downsample the residual path
                residual_path1 = AveragePooling2D(pool_size=(3, 3), strides=1, padding='same')(residual_path1)
                residual_path2 = AveragePooling2D(pool_size=(3, 3), strides=1, padding='same')(residual_path2)

                # zero pad to increase channels
                desired_channels = conv_path1.shape.as_list()[1]

                padding1 = Lambda(pad_depth, arguments={'desired_channels': desired_channels})(residual_path1)
                residual_path1 = keras.layers.Concatenate(axis=1)([residual_path1, padding1])

                padding2 = Lambda(pad_depth, arguments={'desired_channels': desired_channels})(residual_path2)
                residual_path2 = keras.layers.Concatenate(axis=1)([residual_path2, padding2])

            residual_path1 = keras.layers.add([conv_path1, residual_path1])
            residual_path2 = keras.layers.add([conv_path2, residual_path2])

        # when we are here, we double the number of filters
        num_filters *= 2
        # mix down the time dim
        if stack == 0:
            residual_path1 = MaxPooling2D(pool_size=(5, 1), padding='same')(residual_path1)
            residual_path2 = MaxPooling2D(pool_size=(5, 1), padding='same')(residual_path2)

    residual_path = Concatenate(axis=-1, name='res_concat')([residual_path1, residual_path2])
    residual_path = BatchNormalization(center=True, scale=True)(residual_path)
    residual_path = Activation('relu')(residual_path)
    model = Model(inputs=inputs, outputs=residual_path)
    model.summary()
    return model


def get_model_resnet(data_in, data_out, dropout_rate, rnn_size, fnn_size, weights, doa_objective):
    # model definition
    spec_start = Input(shape=(data_in[-3], data_in[-2], data_in[-1]))

    # ResNet
    spec_resnet = spec_start
    spec_resnet = resnet(
        num_classes=10,
        input_shape=(data_in[-3], data_in[-2], data_in[-1]),
    )(spec_resnet)
    spec_resnet = Permute((2, 1, 3))(spec_resnet)

    # RNN
    spec_rnn = Reshape((data_out[0][-2], -1))(spec_resnet)
    for nb_rnn_filt in rnn_size:
        spec_rnn = Bidirectional(
            GRU(
                nb_rnn_filt,
                activation='tanh',
                dropout=dropout_rate,
                recurrent_dropout=dropout_rate,
                return_sequences=True
            ),
            merge_mode='mul'
        )(spec_rnn)

    # FC - DOA
    doa = spec_rnn
    for nb_fnn_filt in fnn_size:
        doa = TimeDistributed(Dense(nb_fnn_filt))(doa)
        doa = Dropout(dropout_rate)(doa)

    doa = TimeDistributed(Dense(data_out[1][-1]))(doa)
    doa = Activation('tanh', name='doa_out')(doa)

    # FC - SED
    sed = spec_rnn
    for nb_fnn_filt in fnn_size:
        sed = TimeDistributed(Dense(nb_fnn_filt))(sed)
        sed = Dropout(dropout_rate)(sed)
    sed = TimeDistributed(Dense(data_out[0][-1]))(sed)
    sed = Activation('sigmoid', name='sed_out')(sed)

    model = None
    if doa_objective == 'mse':
        model = Model(inputs=spec_start, outputs=[sed, doa])
        model.compile(optimizer=Adam(), loss=['binary_crossentropy', 'mse'], loss_weights=weights)
    elif doa_objective == 'masked_mse':
        doa_concat = Concatenate(axis=-1, name='doa_concat')([sed, doa])
        model = Model(inputs=spec_start, outputs=[sed, doa_concat])
        model.compile(optimizer=Adam(), loss=['binary_crossentropy', masked_mse], loss_weights=weights)
    else:
        print('ERROR: Unknown doa_objective: {}'.format(doa_objective))
        exit()
    model.summary()
    return model
