# -*- coding: utf-8 -*-
from keras.layers import Bidirectional, Input, Concatenate, AveragePooling2D, Lambda
from keras.layers.core import Dense, Activation, Dropout, Reshape
from keras.layers.recurrent import GRU
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.layers.wrappers import TimeDistributed
from keras.optimizers import Adam
import keras

from .utility import masked_mse, pad_depth
from .layers import resnet_layer

keras.backend.set_image_data_format('channels_last')


def get_model_resnet_qwang(data_in, data_out, dropout_rate, f_pool_size, t_pool_size,
                           rnn_size, wd, fnn_size, num_filters, weights, doa_objective):
    # model definition
    spec_start = Input(shape=(data_in[-3], data_in[-2], data_in[-1]))
    num_res_blocks = 2

    # resnet
    spec_resnet = spec_start
    residual_path = resnet_layer(inputs=spec_resnet,
                                 num_filters=num_filters,
                                 learn_bn=True,
                                 wd=wd,
                                 use_relu=False)
    for i, convCnt in enumerate(f_pool_size):
        for res_block in range(num_res_blocks):
            if i > 0 and res_block == 0:
                conv_path = resnet_layer(
                    inputs=residual_path,
                    num_filters=num_filters,
                    learn_bn=False,
                    wd=wd,
                    use_relu=True,
                    pool_size=(t_pool_size[i - 1], f_pool_size[i - 1])
                )
            else:
                conv_path = resnet_layer(
                    inputs=residual_path,
                    num_filters=num_filters,
                    learn_bn=False,
                    wd=wd,
                    use_relu=True
                )
            conv_path = resnet_layer(
                inputs=conv_path,
                num_filters=num_filters,
                learn_bn=False,
                wd=wd,
                use_relu=True
            )
            if i > 0 and res_block == 0:
                residual_path = AveragePooling2D(pool_size=(t_pool_size[i - 1], f_pool_size[i - 1]), padding='same')(
                    residual_path)
                desired_channels = conv_path.shape.as_list()[-1]
                padding = Lambda(pad_depth, arguments={'desired_channels': desired_channels})(residual_path)
                residual_path = keras.layers.Concatenate(axis=-1)([residual_path, padding])
            residual_path = keras.layers.add([conv_path, residual_path])
        # ResidualPath = MaxPooling2D(pool_size=(t_pool_size[i], f_pool_size[i]), padding='same')(ResidualPath)
        num_filters *= 2

    output_path = BatchNormalization(center=False, scale=False)(residual_path)
    output_path = Activation('relu')(output_path)

    # RNN
    spec_rnn = Reshape((data_out[0][-2], -1))(output_path)
    for nb_rnn_filt in rnn_size:
        spec_rnn = Bidirectional(
            GRU(nb_rnn_filt, activation='tanh', dropout=dropout_rate, recurrent_dropout=dropout_rate,
                return_sequences=True),
            merge_mode='mul'
        )(spec_rnn)

    # FC - DOA
    doa = spec_rnn
    for nb_fnn_filt in fnn_size:
        doa = TimeDistributed(Dense(nb_fnn_filt))(doa)
        doa = Dropout(dropout_rate)(doa)

    doa = TimeDistributed(Dense(data_out[1][-1]))(doa)
    doa = Activation('tanh', name='doa_out')(doa)

    # FC - SED
    sed = spec_rnn
    for nb_fnn_filt in fnn_size:
        sed = TimeDistributed(Dense(nb_fnn_filt))(sed)
        sed = Dropout(dropout_rate)(sed)
    sed = TimeDistributed(Dense(data_out[0][-1]))(sed)
    sed = Activation('sigmoid', name='sed_out')(sed)

    model = None
    if doa_objective == 'mse':
        model = Model(inputs=spec_start, outputs=[sed, doa])
        model.compile(optimizer=Adam(), loss=['binary_crossentropy', 'mse'], loss_weights=weights)
    elif doa_objective == 'masked_mse':
        doa_concat = Concatenate(axis=-1, name='doa_concat')([sed, doa])
        model = Model(inputs=spec_start, outputs=[sed, doa_concat])
        model.compile(optimizer=Adam(), loss=['binary_crossentropy', masked_mse], loss_weights=weights)
    else:
        print('ERROR: Unknown doa_objective: {}'.format(doa_objective))
        exit()
    model.summary()
    return model
