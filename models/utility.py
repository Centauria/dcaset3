# -*- coding: utf-8 -*-
import keras
from keras import backend as K


def masked_mse(y_gt, model_out):
    # SED mask: Use only the predicted DOAs when gt SED > 0.5
    sed_out = y_gt[:, :, :14] >= 0.5  # TODO fix this hardcoded value of number of classes
    sed_out = keras.backend.repeat_elements(sed_out, 3, -1)
    sed_out = keras.backend.cast(sed_out, 'float32')

    # Use the mask to computed mse now. Normalize with the mask weights
    # TODO fix this hardcoded value of number of classes
    return keras.backend.sqrt(
        keras.backend.sum(
            keras.backend.square(y_gt[:, :, 14:] - model_out[:, :, 14:]) * sed_out
        )
    ) / keras.backend.sum(sed_out)


def pad_depth(inputs, desired_channels):
    y = K.zeros_like(inputs, name='pad_depth1')
    return y
