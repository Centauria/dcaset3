# -*- coding: utf-8 -*-
from keras.layers import Bidirectional, Input, Concatenate, SeparableConv2D, Conv2D, MaxPooling2D, add
from keras.layers.core import Dense, Activation, Dropout, Reshape
from keras.layers.recurrent import GRU
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.layers.wrappers import TimeDistributed
from keras.optimizers import Adam
import keras

from .utility import masked_mse, pad_depth
from .layers import sep_conv

keras.backend.set_image_data_format('channels_last')


def get_model_xception_rnn(data_in, data_out, dropout_rate, rnn_size, fnn_size, mid_block_num,
                           block_13, block_14, mid_filter_num, wd,
                           weights, doa_objective):
    # model definition
    spec_start = Input(shape=(data_in[-3], data_in[-2], data_in[-1]))

    # Xception
    spec_cnn = spec_start

    # Block 1
    x = Conv2D(32, (3, 3), strides=(5, 2), use_bias=False)(spec_cnn)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = Conv2D(64, (3, 3), padding='same', use_bias=False)(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    residual = Conv2D(128, (1, 1), strides=(1, 2), padding='same', use_bias=False)(x)
    residual = BatchNormalization()(residual)

    # Block 2
    x = sep_conv(x, 128, wd=wd)
    x = sep_conv(x, 128, activation=None, wd=wd)

    # Block 2 Pool
    x = MaxPooling2D((3, 3), strides=(1, 2), padding='same')(x)
    x = add([x, residual])

    residual = Conv2D(256, (1, 1), strides=(1, 2), padding='same', use_bias=False)(x)
    residual = BatchNormalization()(residual)

    # Block 3
    x = Activation('relu')(x)
    x = sep_conv(x, 256, wd=wd)
    x = sep_conv(x, 256, activation=None, wd=wd)

    # Block 3 Pool
    x = MaxPooling2D((3, 3), strides=(1, 2), padding='same')(x)
    x = add([x, residual])

    residual = Conv2D(mid_filter_num, (1, 1), strides=(1, 2), padding='same', use_bias=False)(x)
    residual = BatchNormalization()(residual)

    # Block 4
    x = Activation('relu')(x)
    x = sep_conv(x, mid_filter_num, wd=wd)
    x = sep_conv(x, mid_filter_num, activation=None, wd=wd)

    x = MaxPooling2D((3, 3), strides=(1, 2), padding='same')(x)
    x = add([x, residual])

    # Block 5 - 12
    for i in range(mid_block_num):
        residual = x

        x = Activation('relu')(x)
        x = sep_conv(x, mid_filter_num, wd=wd)
        x = sep_conv(x, mid_filter_num, wd=wd)
        x = sep_conv(x, mid_filter_num, activation=None, wd=wd)

        x = add([x, residual])

    residual = Conv2D(1024, (1, 1), strides=(1, 2), padding='same', use_bias=False)(x)
    residual = BatchNormalization()(residual)

    if block_13:
        # Block 13
        x = Activation('relu')(x)
        x = sep_conv(x, mid_filter_num, wd=wd)
        x = sep_conv(x, 1024, activation=None, wd=wd)

        # Block 13 Pool
        x = MaxPooling2D((3, 3), strides=(1, 2), padding='same')(x)
        x = add([x, residual])

    if block_14:
        # Block 14
        x = sep_conv(x, 1536, wd=wd)

        # Block 14 part 2
        x = sep_conv(x, 2048, wd=wd)

    # RNN
    spec_rnn = Reshape((data_out[0][-2], -1))(x)
    for nb_rnn_filt in rnn_size:
        spec_rnn = Bidirectional(
            GRU(nb_rnn_filt, activation='tanh', dropout=dropout_rate, recurrent_dropout=dropout_rate,
                return_sequences=True),
            merge_mode='mul'
        )(spec_rnn)

    # FC - DOA
    doa = spec_rnn
    for nb_fnn_filt in fnn_size:
        doa = TimeDistributed(Dense(nb_fnn_filt))(doa)
        doa = Dropout(dropout_rate)(doa)

    doa = TimeDistributed(Dense(data_out[1][-1]))(doa)
    doa = Activation('tanh', name='doa_out')(doa)

    # FC - SED
    sed = spec_rnn
    for nb_fnn_filt in fnn_size:
        sed = TimeDistributed(Dense(nb_fnn_filt))(sed)
        sed = Dropout(dropout_rate)(sed)
    sed = TimeDistributed(Dense(data_out[0][-1]))(sed)
    sed = Activation('sigmoid', name='sed_out')(sed)

    model = None
    if doa_objective == 'mse':
        model = Model(inputs=spec_start, outputs=[sed, doa])
        model.compile(optimizer=Adam(), loss=['binary_crossentropy', 'mse'], loss_weights=weights)
    elif doa_objective == 'masked_mse':
        doa_concat = Concatenate(axis=-1, name='doa_concat')([sed, doa])
        model = Model(inputs=spec_start, outputs=[sed, doa_concat])
        model.compile(optimizer=Adam(), loss=['binary_crossentropy', masked_mse], loss_weights=weights)
    else:
        print('ERROR: Unknown doa_objective: {}'.format(doa_objective))
        exit()
    model.summary()
    return model
