# Parameters used in the feature extraction, neural network model, and training the SELDnet can be changed here.
#
# Ideally, do not change the values of the default parameters. Create separate cases with unique <task-id> as seen in
# the code below (if-else loop) and use them. This way you can easily reproduce a configuration on a later time.
import os
import yaml
from pyhocon import ConfigFactory, HOCONConverter

config_path = 'configs'


def get_params(argv='default'):
    print("SET: {}".format(argv))
    # ########### default parameters ##############
    params = dict(
        quick_test=False,  # To do quick test. Trains/test on small subset of dataset, and # of epochs

        # INPUT PATH
        dataset_dir='/disk1/Centauria/datasets/DCASE/data2020/',
        # Base folder containing the foa/mic and metadata folders

        # OUTPUT PATH
        feat_label_dir='/disk1/Centauria/datasets/DCASE/data2020/feat_label/',
        # Directory to dump extracted features and labels
        model_dir='models/',  # Dumps the trained models and training curves in this folder
        dcase_output=True,  # If true, dumps the results recording-wise in 'dcase_dir' path.
        # Set this true after you have finalized your model, save the output, and submit
        dcase_dir='results/',  # Dumps the recording-wise network output in this folder

        # DATASET LOADING PARAMETERS
        mode='dev',  # 'dev' - development or 'eval' - evaluation dataset
        dataset='foa',  # 'foa' - ambisonic or 'mic' - microphone signals
        channel_first=True,

        # FEATURE PARAMS
        fs=24000,
        hop_len_s=0.02,
        label_hop_len_s=0.1,
        max_audio_len_s=60,
        nb_mel_bins=64,

        # DNN MODEL PARAMETERS
        label_sequence_length=60,  # Feature sequence length
        batch_size=256,  # Batch size
        dropout_rate=0,  # Dropout rate, constant for all layers
        num_filters=64,  # Number of CNN nodes, constant for each layer
        f_pool_size=[4, 4, 2],
        # CNN frequency pooling, length of list = number of CNN layers, list value = pooling per layer

        rnn_size=[128, 128],  # RNN contents, length of list = number of layers, list value = number of nodes
        fnn_size=[128],  # FNN contents, length of list = number of layers, list value = number of nodes
        loss_weights=[1., 1000.],  # [sed, doa] weight for scaling the DNN outputs
        nb_epochs=50,  # Train for maximum epochs
        epochs_per_fit=5,  # Number of epochs per fit
        doa_objective='masked_mse',
        # supports: mse, masked_mse. mse- original seld approach; masked_mse - dcase 2020 approach

        # METRIC PARAMETERS
        lad_doa_thresh=20,

        # MODEL
        model='seld',
    )
    feature_label_resolution = int(params['label_hop_len_s'] // params['hop_len_s'])
    params['feature_sequence_length'] = params['label_sequence_length'] * feature_label_resolution
    params['t_pool_size'] = [feature_label_resolution, 1, 1]  # CNN time pooling
    params['patience'] = int(params['nb_epochs'])  # Stop training if patience is reached

    params['unique_classes'] = {
        'alarm': 0,
        'baby': 1,
        'crash': 2,
        'dog': 3,
        'engine': 4,
        'female_scream': 5,
        'female_speech': 6,
        'fire': 7,
        'footsteps': 8,
        'knock': 9,
        'male_scream': 10,
        'male_speech': 11,
        'phone': 12,
        'piano': 13
    }

    # ########### User defined parameters ##############
    if argv == 'default':
        pass

    elif argv == '2':
        params['mode'] = 'dev'
        params['dataset'] = 'mic'

    elif argv == '3':
        params['mode'] = 'eval'
        params['dataset'] = 'mic'

    elif argv == '4':
        params['mode'] = 'dev'
        params['dataset'] = 'foa'

    elif argv == '5':
        params['mode'] = 'eval'
        params['dataset'] = 'foa'

    elif argv == 'resnet':
        params['model'] = 'resnet-rnn'
        params['feat_label_dir'] = '/disk1/Centauria/datasets/DCASE/data2020/feat_label_128d/'
        params['nb_mel_bins'] = 128
        params['batch_size'] = 32

    elif argv == 'resnet-qw':
        params['model'] = 'resnet-rnn-qwang'
        params['mode'] = 'dev'
        params['dataset'] = 'foa'
        params['channel_first'] = False
        params['batch_size'] = 128
        params['num_filters'] = 24
        params['f_pool_size'] = [4, 4, 2, 1]
        params['t_pool_size'] = [feature_label_resolution, 1, 1, 1]
        params['rnn_size'] = [512, 512]
        params['fnn_size'] = [512]
        params['wd'] = 1e-3

    elif argv == 'resnet-qw-filt=12':
        params['model'] = 'resnet-rnn-qwang'
        params['mode'] = 'dev'
        params['dataset'] = 'foa'
        params['channel_first'] = False
        params['batch_size'] = 256
        params['num_filters'] = 12
        params['f_pool_size'] = [4, 4, 2, 1]
        params['t_pool_size'] = [feature_label_resolution, 1, 1, 1]
        params['rnn_size'] = [512, 512]
        params['fnn_size'] = [512]
        params['wd'] = 1e-3

    elif argv == 'resnet-qw-filt=36':
        params['model'] = 'resnet-rnn-qwang'
        params['mode'] = 'dev'
        params['dataset'] = 'foa'
        params['channel_first'] = False
        params['batch_size'] = 128
        params['num_filters'] = 36
        params['f_pool_size'] = [4, 4, 2, 1]
        params['t_pool_size'] = [feature_label_resolution, 1, 1, 1]
        params['rnn_size'] = [512, 512]
        params['fnn_size'] = [512]
        params['wd'] = 1e-3

    elif argv == 'quick':
        print("QUICK TEST MODE\n")
        params['quick_test'] = True
        params['epochs_per_fit'] = 1

    else:
        print('ERROR: unknown argument {}'.format(argv))
        exit()

    for key, value in params.items():
        print("\t{}: {}".format(key, value))
    return params


def load_parameters(preset='default'):
    params = ConfigFactory.parse_file(os.path.join(config_path, preset + '.conf'))
    for key, value in params.items():
        print("\t{}: {}".format(key, value))
    return params


def save_parameters(params, preset='default'):
    with open(os.path.join(config_path, preset + '.yaml'), 'w') as f:
        f.write(HOCONConverter.convert(params, 'hocon'))
