#
# A wrapper script that trains the SELDnet.
# The training stops when the early stopping metric - SELD error stops improving.
#

import os
import sys
import re
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import cls_feature_class
import cls_data_generator
import models
import parameter
import time
import argparse
from keras import backend as K

from metrics import evaluation_metrics, SELD_evaluation_metrics
from util import collect_test_labels, metric_scores, show_result

plt.switch_backend('agg')

seed = np.random.randint(0, 1000)
np.random.seed(seed)
tf.random.set_seed(seed)


def plot_functions(fig_name, _tr_loss, _sed_loss, _doa_loss, _epoch_metric_loss, _new_metric, _new_seld_metric):
    plt.figure()
    nb_epoch = len(_tr_loss)
    plt.subplot(411)
    plt.plot(range(nb_epoch), _tr_loss, label='train loss')
    plt.legend()
    plt.grid(True)

    plt.subplot(412)
    plt.plot(range(nb_epoch), _sed_loss[:, 0], label='sed er')
    plt.plot(range(nb_epoch), _sed_loss[:, 1], label='sed f1')
    plt.plot(range(nb_epoch), _doa_loss[:, 0] / 180., label='doa er / 180')
    plt.plot(range(nb_epoch), _doa_loss[:, 1], label='doa fr')
    plt.plot(range(nb_epoch), _epoch_metric_loss, label='seld')
    plt.legend()
    plt.grid(True)

    plt.subplot(413)
    plt.plot(range(nb_epoch), _new_metric[:, 0], label='seld er')
    plt.plot(range(nb_epoch), _new_metric[:, 1], label='seld f1')
    plt.plot(range(nb_epoch), _new_metric[:, 2] / 180., label='doa er / 180')
    plt.plot(range(nb_epoch), _new_metric[:, 3], label='doa fr')
    plt.plot(range(nb_epoch), _new_seld_metric, label='seld')

    plt.legend()
    plt.grid(True)

    plt.subplot(414)
    plt.plot(range(nb_epoch), _doa_loss[:, 2], label='pred_pks')
    plt.plot(range(nb_epoch), _doa_loss[:, 3], label='good_pks')
    plt.legend()
    plt.grid(True)

    plt.savefig(fig_name)
    plt.close()


def main(args, params_addon):
    """
    Main wrapper for training sound event localization and detection network.
    
    :param args: expects two optional inputs.
        first input: task_id - (optional) To chose the system configuration in parameters.py.
                                (default) 1 - uses default parameters
        second input: job_id - (optional) all the output files will be uniquely represented with this.
                              (default) 1

    """
    print(args)
    print('seed={}'.format(seed))

    # use parameter set defined by user
    params = parameter.load_parameters(args.task_id)
    for k, v in params_addon.items():
        params[k] = v

    feat_cls = cls_feature_class.FeatureClass(params)
    train_splits, val_splits, test_splits = None, None, None

    if params['mode'] == 'dev':
        test_splits = [1]
        val_splits = [2]
        train_splits = [[3, 4, 5, 6]]

    elif params['mode'] == 'eval':
        test_splits = [[7, 8]]
        val_splits = [[1]]
        train_splits = [[2, 3, 4, 5, 6]]

    avg_scores_val = []
    for split_cnt, split in enumerate(test_splits):
        print('\n\n---------------------------------------------------------------------------------------------------'
              '\n------------------------------------      SPLIT {}   -----------------------------------------------'
              '\n---------------------------------------------------------------------------------------------------'
              '\n'.format(split))

        # Unique name for the run
        cls_feature_class.create_folder(params['model_dir'])
        unique_name = '{}_{}_{}_{}_split{}'.format(
            args.task_id, args.job_id, params['dataset'], params['mode'], split
        )
        unique_name = os.path.join(params['model_dir'], unique_name)
        model_name = '{}_model.h5'.format(unique_name)
        print("unique_name: {}\n".format(unique_name))

        # Load train and validation data
        print('Loading training dataset:')
        data_gen_train = cls_data_generator.DataGenerator(
            params=params, split=train_splits[split_cnt]
        )

        print('Loading validation dataset:')
        data_gen_val = cls_data_generator.DataGenerator(
            params=params, split=val_splits[split_cnt], shuffle=False
        )

        # Collect the reference labels for validation data
        data_in, data_out = data_gen_train.get_data_sizes()
        print('FEATURES:\n\tdata_in: {}\n\tdata_out: {}\n'.format(data_in, data_out))

        nb_classes = data_gen_train.get_nb_classes()
        gt = collect_test_labels(data_gen_val, data_out, nb_classes, params['quick_test'])
        sed_gt = evaluation_metrics.reshape_3Dto2D(gt[0])
        doa_gt = evaluation_metrics.reshape_3Dto2D(gt[1])

        print('MODEL:')
        for k in params.keys():
            print('  {}: {}'.format(k, params[k]))

        if not args.load:
            model = models.get_model(name=params['model'], data_in=data_in, data_out=data_out, params=params)
            best_seld_metric = np.inf
            best_epoch = -1
            patience_cnt = 0
            nb_epoch = 2 if params['quick_test'] else params['nb_epochs']
            seld_metric = np.zeros(nb_epoch)
            new_seld_metric = np.zeros(nb_epoch)
            tr_loss = np.zeros(nb_epoch)
            doa_metric = np.zeros((nb_epoch, 6))
            sed_metric = np.zeros((nb_epoch, 2))
            new_metric = np.zeros((nb_epoch, 4))

            # start training
            for epoch_cnt in range(nb_epoch):
                start = time.time()

                # train once per epoch
                hist = model.fit_generator(
                    generator=data_gen_train.generate(),
                    steps_per_epoch=2 if params['quick_test'] else data_gen_train.get_total_batches_in_data(),
                    epochs=params['epochs_per_fit'],
                    verbose=2,
                )
                tr_loss[epoch_cnt] = hist.history.get('loss')[-1]

                # predict once per epoch
                pred = model.predict_generator(
                    generator=data_gen_val.generate(),
                    steps=2 if params['quick_test'] else data_gen_val.get_total_batches_in_data(),
                    verbose=2
                )

                sed_pred = evaluation_metrics.reshape_3Dto2D(pred[0]) > params['dyn_thresh']
                doa_pred = evaluation_metrics.reshape_3Dto2D(
                    pred[1] if params['doa_objective'] is 'mse' else pred[1][:, :, nb_classes:])

                # Calculate the DCASE 2019 metrics - Detection-only and Localization-only scores
                sed_metric[epoch_cnt, :] = evaluation_metrics.compute_sed_scores(sed_pred, sed_gt,
                                                                                 data_gen_val.nb_frames_1s())
                doa_metric[epoch_cnt, :] = evaluation_metrics.compute_doa_scores_regr_xyz(doa_pred, doa_gt, sed_pred,
                                                                                          sed_gt)
                seld_metric[epoch_cnt] = evaluation_metrics.early_stopping_metric(sed_metric[epoch_cnt, :],
                                                                                  doa_metric[epoch_cnt, :])

                # Calculate the DCASE 2020 metrics - Location-aware detection and Class-aware localization scores
                cls_new_metric = SELD_evaluation_metrics.SELDMetrics(nb_classes=data_gen_val.get_nb_classes(),
                                                                     doa_threshold=params['lad_doa_thresh'])
                pred_dict = feat_cls.regression_label_format_to_output_format(
                    sed_pred, doa_pred
                )
                gt_dict = feat_cls.regression_label_format_to_output_format(
                    sed_gt, doa_gt
                )

                pred_blocks_dict = feat_cls.segment_labels(pred_dict, sed_pred.shape[0])
                gt_blocks_dict = feat_cls.segment_labels(gt_dict, sed_gt.shape[0])

                cls_new_metric.update_seld_scores_xyz(pred_blocks_dict, gt_blocks_dict)
                new_metric[epoch_cnt, :] = cls_new_metric.compute_seld_scores()
                new_seld_metric[epoch_cnt] = evaluation_metrics.early_stopping_metric(new_metric[epoch_cnt, :2],
                                                                                      new_metric[epoch_cnt, 2:])

                # Visualize the metrics with respect to epochs
                plot_functions(unique_name, tr_loss, sed_metric, doa_metric, seld_metric, new_metric, new_seld_metric)

                patience_cnt += 1
                if new_seld_metric[epoch_cnt] < best_seld_metric:
                    best_seld_metric = new_seld_metric[epoch_cnt]
                    best_epoch = epoch_cnt
                    model.save(model_name)
                    patience_cnt = 0

                print(
                    'epoch_cnt: {}, time: {:0.2f}s, tr_loss: {:0.2f}, '
                    '\n\t\t DCASE2019 SCORES: ER: {:0.2f}, F: {:0.1f}, DE: {:0.1f}, FR:{:0.1f}, seld_score: {:0.2f}, '
                    '\n\t\t DCASE2020 SCORES: ER: {:0.2f}, F: {:0.1f}, DE: {:0.1f}, DE_F:{:0.1f}, '
                    'seld_score (early stopping score): {:0.2f}, '
                    'best_seld_score: {:0.2f}, best_epoch : {}\n'.format(
                        epoch_cnt, time.time() - start, tr_loss[epoch_cnt],
                        sed_metric[epoch_cnt, 0], sed_metric[epoch_cnt, 1] * 100,
                        doa_metric[epoch_cnt, 0], doa_metric[epoch_cnt, 1] * 100, seld_metric[epoch_cnt],
                        new_metric[epoch_cnt, 0], new_metric[epoch_cnt, 1] * 100,
                        new_metric[epoch_cnt, 2], new_metric[epoch_cnt, 3] * 100,
                        new_seld_metric[epoch_cnt], best_seld_metric, best_epoch
                    )
                )
                if patience_cnt > params['patience']:
                    lr = K.get_value(model.optimizer.lr)
                    K.set_value(model.optimizer.lr, lr * 0.1)
                    patience_cnt = 0
                    print('Learning rate: {}'.format(K.get_value(model.optimizer.lr)))

            avg_scores_val.append([new_metric[best_epoch, 0], new_metric[best_epoch, 1], new_metric[best_epoch, 2],
                                   new_metric[best_epoch, 3], best_seld_metric])
            print('\nResults on validation split:')
            print('\tUnique_name: {} '.format(unique_name))
            print('\tSaved model for the best_epoch: {}'.format(best_epoch))
            print('\tSELD_score (early stopping score) : {}'.format(best_seld_metric))

            print('\n\tDCASE2020 scores')
            print('\tClass-aware localization scores: DOA_error: {:0.1f}, F-score: {:0.1f}'
                  ''.format(new_metric[best_epoch, 2], new_metric[best_epoch, 3] * 100))
            print('\tLocation-aware detection scores: Error rate: {:0.2f}, F-score: {:0.1f}'
                  ''.format(new_metric[best_epoch, 0], new_metric[best_epoch, 1] * 100))

            print('\n\tDCASE2019 scores')
            print('\tLocalization-only scores: DOA_error: {:0.1f}, Frame recall: {:0.1f}'
                  ''.format(doa_metric[best_epoch, 0], doa_metric[best_epoch, 1] * 100))
            print('\tDetection-only scores: Error rate: {:0.2f}, F-score: {:0.1f}\n'
                  ''.format(sed_metric[best_epoch, 0], sed_metric[best_epoch, 1] * 100))

        # ------------------  Calculate metric scores for unseen test split ---------------------------------
        print('\nLoading the best model and predicting results on the testing split')
        print('\tLoading testing dataset:')

        data_gen_test = cls_data_generator.DataGenerator(
            params=params, split=split, shuffle=False, per_file=params['dcase_output'],
            is_eval=True if params['mode'] is 'eval' else False
        )

        model = models.load_seld_model('{}_model.h5'.format(unique_name), params['doa_objective'])

        metric_scores(model, data_gen_test, params, feat_cls, args.task_id, args.job_id)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('task_id', default='default', help='User-defined parameter set from parameter.py')
    parser.add_argument('job_id', default='1',
                        help='Unique identifier which is used for output filenames (models, training plots).'
                             + os.linesep +
                             'You can use any number or string for this.')
    parser.add_argument('-l', '--load', action='store_true', help='Load trained model')
    parser.add_argument('-p', '--params', nargs='*', help='Temporary params, add/overrides params in the *.conf file.'
                                                          + os.linesep +
                                                          'example: dyn_thresh:float=0.75')
    args = parser.parse_args()
    params_addon = dict()
    for item in args.params:
        m = re.match(r'(.*):(.*)=(.*)', item)
        k, t, v = m.groups()
        if t in ('int', 'float', 'str'):
            params_addon[k] = eval(t)(v)
        elif t in ('list', 'tuple'):
            params_addon[k] = eval(t)(eval(v))
    try:
        main(args, params_addon)
    except (ValueError, IOError) as e:
        sys.exit(e)
