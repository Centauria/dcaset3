# -*- coding: utf-8 -*-
import os
import argparse
from pyhocon import ConfigFactory, HOCONConverter
import parameter

parser = argparse.ArgumentParser()
parser.add_argument('task_id',
                    help='is used to choose the user-defined parameter set from parameter.py')
parser.add_argument('job_id',
                    help='is a unique identifier which is used for output filenames (models, training plots). '
                         'You can use any number or string for this.')
parser.add_argument('override', nargs=argparse.REMAINDER)
args = parser.parse_args()

print(args)
params = ConfigFactory.parse_file(os.path.join('configs', args.task_id + '.conf'))
for p in args.override:
    k, v = p.split('=')
    k = k.lstrip('-')
    k = k.replace('-', '_')
    chain = k.split(':')
    ps = params
    for i in range(len(chain) - 1):
        ps = ps[chain[i]]
    ps[chain[-1]] = v
params = ConfigFactory.parse_string(HOCONConverter.convert(params))
print(params)
